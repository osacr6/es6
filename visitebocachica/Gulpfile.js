'use strict';
  //Packages
const gulp = require('gulp'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  pump = require('pump'),
  cleanCSS = require('gulp-clean-css'),
  sourcemaps = require('gulp-sourcemaps'),
  htmlhint = require('gulp-htmlhint'),
  htmlmin = require('gulp-htmlmin'),
  ngAnnotate = require('gulp-ng-annotate'),
  ngTemplates = require('gulp-ng-templates'),
  path = require('path'),
  del = require('del'),
  browserSync = require('browser-sync').create(),

  eslint = require('gulp-eslint');

//Paths
var src = './public',
    src_js = path.join(src, '/javascripts'),
    src_css = path.join(src, '/stylesheets'),
    src_js_vendor = path.join(src_js, '/vendor'),
    src_css_vendor = path.join(src_css, '/vendor'),
    src_js_site = path.join(src_js, '/site'),
    src_css = path.join(src, '/stylesheets'),
    dist = './dist',
    dist_js = path.join(dist, '/js'),
    dist_css = path.join(dist, '/css');

/**
 * Gulp task to compile SaSS into CSS.
 */
gulp.task('sass', function () {
 return gulp.src(
  [
    'public/stylesheets/style.scss'
  ])
  .pipe(sourcemaps.init())
  .pipe(sass().on('error', sass.logError))
  .pipe(gulp.dest(dist_css));
});



// Clean folders
gulp.task('clean', function () {
    'use strict';
    return del.sync([
      dist_js,
      dist_css,
      './public/javascripts/site/templates-angular.js'
     ]);
});

gulp.task('eslint', function () {
    'use strict';
    return gulp.src([
        path.join(src_js_site, '/{,**/}*.js'),
        '!' + path.join(src_js_site, '/templates-angular.js')
    ])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failOnError());
});

// HTMLHint, Code Verifier
gulp.task('htmlhint', function () {
    'use strict';
    return gulp.src([
        path.join(src, '/{,**/}*.html'),
        path.join(src_js_site, '/{,**/}*.html')
    ])
    .pipe(htmlhint('.htmlhintrc'))
    .pipe(htmlhint.failReporter());
});

// AngularJS DI Annotation
gulp.task('_ngAnnotate', ['concat:site'], function () {
    'use strict';
    return gulp.src(
        path.join(dist_js, 'site.js')
    )
    .pipe(ngAnnotate())
    .pipe(gulp.dest(dist_js));
});

// JavaScript Minimizer
gulp.task('compress:js', ['concat:site'], function (cb) {
    'use strict';
    return pump([
        gulp.src([
          path.join(dist_js, '/libraries.js'),
          path.join(dist_js, '/site.js')
        ]),
        uglify(),
        gulp.dest(dist_js)
    ], cb);
});

// CSS Minimizer
gulp.task('compress:css', ['sass'], function() {
    'use strict';
    return gulp.src([
        path.join(dist_css, '/{,**/}*.css')
    ])
    .pipe(sourcemaps.init())
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(dist_css));
});

// Angular Templates Compiler (HTML to JS)
gulp.task('ngtemplates', function () {
    'use strict';
    return gulp.src(
        path.join(src_js_site, '/{,**/}/views/*.html')
    )
    .pipe(ngTemplates({
        filename: 'templates-angular.js',
        module: 'bocachicaApp.templates',
        standalone: true
    }))
    .pipe(gulp.dest(src_js_site));
});

// File Concatenation for JavaScript files
gulp.task('concat:libraries', function() {
    'use strict';
    return gulp.src([
      path.join(src_js_vendor, '/jquery-3.1.1.min.js'),
      path.join(src_js_vendor, '/angular/angular.min.js'),
      path.join(src_js_vendor, '/{,**/}*.js'),
      path.join(src_js_vendor, '/{,**/}*.*.js')
    ])
    .pipe(concat('libraries.js'))
    .pipe(gulp.dest(dist_js));
});

// File Concatenation for css files
gulp.task('concat:styles', function() {
    'use strict';
    return gulp.src([
      path.join(src_css_vendor, '/{,**/}*.css'),
      path.join(src_css_vendor, '/{,**/}*.*.css')
    ])
    .pipe(concat('libraries.css'))
    .pipe(gulp.dest(dist_css));
});


gulp.task('concat:site', ['ngtemplates'], function() {
    'use strict';
    return gulp.src([
        path.join(src_js_site, '/{,**/}*util.js'),
        path.join(src_js_site, '/{,**/}*.module.js'),
        path.join(src_js_site, '/{,**/}*.js'),
        path.join(src_js_site, '/{,**/}*.*.js'),
        path.join(src_js_site, '/templates-angular.js'),
        path.join(src_js_site, '/app.js')
    ])
    .pipe(concat('site.js'))
    .pipe(gulp.dest(dist_js));
});

// Helper Tasks
gulp.task('linter', ['eslint', 'htmlhint']);
gulp.task('build:js', ['sass','ngtemplates', 'concat:libraries', 'concat:styles', 'concat:site']); //, 'ngAnnotate'
gulp.task('build:dev', ['clean', 'build:js']);
gulp.task('build:optimize', ['compress:js', 'compress:css']);
gulp.task('build:prod', ['clean', 'linter', 'build:dev', 'build:optimize']);

/**
 * Gulp task to watch for changes and update JCR on the fly.
 */
gulp.task('watch',['build:dev'] ,function () {
  // browserSync.init({
  //   reloadDelay: 2000,
  //   proxy: "localhost:5000",
  //   port: 5000,
  //   ui: false
  // });

  gulp.watch(
    path.join(src_css, '/{,**/}*.scss'), 
  ['sass']);

  gulp.watch([
    path.join(src_js, '/{,**/}*.js')
  ], ['build:dev']);

});

gulp.task('build', ['build:prod']);
