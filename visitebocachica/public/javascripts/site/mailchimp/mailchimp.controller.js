/**
 * @ngdoc Controller
 * @name chatBotApp.admin
 * @description # admin Controller
 *
 * @requieres
 */

angular.module('bocachicaApp.mailchimp')
//.controller('mailchimpController', ["$scope", "$q", "$http", function ($scope, $q, $http) {
.controller('mailchimpController', function ($scope, $q, $http) {
    'user strict';

    var self = this;

    self.sendContactMail = function () {
        return $q(function (resolve, reject) {
            var req = {
                method: 'POST',
                url: '/contact',
                data: { 
                    name: self.name,
                    email: self.email,
                    message: self.message
                }
            };
            $http(req)
            .then(function (response) {
                if (response.status === 200) {
                    $.notify(response.data.message,"success");
                } else {
                    $.notify(response.status,"success");
                }
            }, function (err) {
                $.notify("Error","error");
            });
        });
    };
});
//}]);