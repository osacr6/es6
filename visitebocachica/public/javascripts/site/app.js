/**
 * @ngdoc module
 * @name bocachicaApp
 * @description # module
 */

angular.module('bocachicaApp', [
	'bocachicaApp.mailchimp'
]);