// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// use npm http-server

'use strict';

const STORAGE_KEY = 'vtestconf';


const   WAITFOR = 1500; 

const _vConf = {
  "v": {
    "name": "",
    "url": "",
    "file": "",
    "device": "",
    "events": []
  }
};

const _workflow = {
  "query": "",
  "click": "false", 
  "value": ""
};


/***/
const workflowHTML = (events) => {
  let workflow = '';

  events.reduce((result, item) => {
    workflow += `<fieldset class="vtest-event">
      <label>css query</label>
      <input type="text" name="query" value="${item.query}">
    
      <label>value</label>
      <input type="text" name="value" value="${item.value}">

      <label><input type="checkbox" name="click" value="click" ${item.click == 'true' ? 'checked' : ''}>click</label>

      <button class="vtest-event-delete">Remove</button>
    </fieldset>`;

    return result;
  }, 0);


  return workflow;
};

/**
 * 
 * @param {*} vConf 
 */
const fieldsetHTML = (key, vConf) => {
  const workflow = vConf.events.length > 0 ? workflowHTML(vConf.events) : '';

  return `<fieldset class="${key}">
    <legend><input type="text" name="name" value="${vConf.name}"></legend>

    <label>Url</label>
    <input type="text" name="url" value="${vConf.url}"> 

    <label>File</label>
    <input type="text" name="file" value="${vConf.file}">

    <label>Device</label>
    <select name="devices">
      <option value="320">320</option>
    </select>

    <p>Build Your Workflow</p>
    <div class="vtest-workflow">

      ${vConf.events.length > 0 ?  workflow : ''}
    
    </div>

    <button class="vtest-workflow-add">Add</button> 
    <button class="vtest-delete">Delete</button> 
    <button class="vtest-run">Run</button>
  </fieldset>`;
}

/**
 * 
 * @param {url} obj 
 */
let request = obj => {
  return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.open(obj.method || "GET", obj.url);
      if (obj.headers) {
        Object.keys(obj.headers).forEach(key => {
          xhr.setRequestHeader(key, obj.headers[key]);
        });
      }
      xhr.onload = () => {
        if (xhr.status >= 200 && xhr.status < 300) {
          resolve(xhr.response);
        } else {
          reject(xhr.statusText);
        }
      };
      xhr.onerror = () => reject(xhr.statusText);
      xhr.send(obj.body);
  })
}

/**
 * 
 * @param {filepath} filepath 
 */
function getDirectoryEntry(filepath) {
  return new Promise(resolve => {
    chrome.runtime.getPackageDirectoryEntry(function(root) {
      root.getFile(filepath, {}, function(fileEntry) {
        fileEntry.file(function(file) {
          var reader = new FileReader();
          reader.onloadend = function(e) {
            resolve(this.result);
          }
          reader.readAsText(file);
        })
      })
    })
  })
}

/**
 * 
 */
function savejson() {
  const variations = [].slice.call(document.querySelectorAll('.vtest-variations > fieldset'));
  let jsonString = '{';

  for (let i = 0; i < variations.length; i++) {
    const name = variations[i].querySelector('input[name="name"]').value;
    const url = variations[i].querySelector('input[name="url"]').value;
    const file = variations[i].querySelector('input[name="file"]').value;
    const events = variations[i].querySelectorAll('.vtest-event');
    let eventsList = '';

   [].forEach.call(events, function(event, i) {
        const query = event.querySelector('input[name="query"]').value;
        const valueField = event.querySelector('input[name="value"]').value;
        const eventClick = event.querySelector('input[name="click"]');

        eventsList +=  `{"query": "${query}",  "click": "${eventClick.checked ? true : false}",   "value": "${valueField}"}${ i+1 < events.length ? ',':''}`;
   });
    
    jsonString += `"v${i}": {
      "name": "${name}",
      "url": "${url}",
      "file": "${file}",
      "device": "",
      "events":[${eventsList}]
    }${i+1 < variations.length ? ",": ""}`;
  }

  setTimeout(() => {
    jsonString += '}';
    console.log(jsonString);
    localStorage.setItem(STORAGE_KEY, jsonString);
  }, 200);
}

function getEvents(query = 'body', eventType = 'click', value = '') {
  const eventsList = {
    'step': function(query, step) {
      return `(function() {
        new Promise(function(resolve, reject) {
          console.log("document.querySelectorAll(${query})");
          var elements = document.querySelectorAll("${query}");

          if(elements.length > 0) {
            [].forEach.call(elements, function(element) {
              console.log(element);

              const y = element.getBoundingClientRect().top + window.scrollY;
              setTimeout( function() {
                window.scroll({
                  top: y,
                  behavior: 'smooth'
                })
                ${step}
              }, ${WAITFOR});
            });
            return;
          }

          var observer = new MutationObserver(function(mutations) {
            mutations.forEach(function(mutation) {
              var nodes = Array.from(mutation.addedNodes);
              for(var node of nodes) {
                var elements = document.querySelectorAll("${query}");
                if(elements.length > 0) {
                  observer.disconnect();
                  
                  [].forEach.call(elements, function(element) {
                    console.log(element);

                    const y = element.getBoundingClientRect().top + window.scrollY;
                    setTimeout( function() {
                      window.scroll({
                        top: y,
                        behavior: 'smooth'
                      })
                      ${step}
                    }, ${WAITFOR});
                  });
                  return;
                }
              }
            })
          })

          observer.observe(document, { childList: true, subtree: true });
        })

      }());`;
    },

    'click': `element.click();`,
    'value': `if (element.constructor.name == 'HTMLInputElement') { element.value = "${value}" }` // document.createElement('input').constructor.name
  };

  return eventsList['step'](query, eventsList[eventType]);
};

/***/
function getworkflow(field) {
  const fieldsetList = field.querySelectorAll('.vtest-event');
  let eventsList = '';

  [].forEach.call(fieldsetList, function(fieldset) {
    const query = fieldset.querySelector('input[name="query"]').value;
    const valueField = fieldset.querySelector('input[name="value"]').value;
    const eventClick = fieldset.querySelector('input[name="click"]');
    
    eventClick.checked ?  eventsList += getEvents(query, 'click') :  eventsList += getEvents(query, 'value', valueField);
 })

  return eventsList;
}

/**
 * 
 * @param {*} url 
 * @param {*} bundle 
 */
function openIncognito(url, bundle, workflow) {
  chrome.windows.create({
    'url': url,
    'focused': true,
    'incognito': true,
    'state': 'maximized'
  },function(w) {
    const t = w.tabs[0];

    if(!bundle) {

      console.log('no bundle');
      chrome.tabs.executeScript(t.id, {'code': `${workflow}`, 'runAt': 'document_end'});
    } else {
      request({'url': bundle})
      .then(data => {

        console.log('bundle data');
        chrome.tabs.executeScript(t.id, {'code': `${data}${workflow}`, 'runAt': 'document_end'});
      })
      .catch(error => {
        console.log(error);
      })
    }
  })
}



/**
 * 
 */
function runBtn() {
  const runBtn = [].slice.call(document.querySelectorAll('.vtest-run'));
  for (let i = 0; i < runBtn.length; i++) {
    try {
      runBtn[i].removeEventListener('click', (e) => {}, true);
    } catch(error){
    }
    runBtn[i].addEventListener('click', function({target}) {
      const field = target.closest('fieldset');
      const url = field.querySelector('input[name="url"]').value;
      const file = field.querySelector('input[name="file"]').value;
      const workflow = getworkflow(field);

      //console.log('workflow');
     //console.log(workflow);

      openIncognito(url, file, workflow);
    });
  }
}

/**
 * 
 */
function deleteBtn() {
  const deleteBtn =[].slice.call(document.querySelectorAll('.vtest-delete'));
  for (let i = 0; i < deleteBtn.length; i++) {
    try {
      runBtn[i].removeEventListener('click', (e) => {}, true);
    } catch(error){
    }
    deleteBtn[i].addEventListener('click', function({target}) {
      target.closest('fieldset').remove()
    })
  }
}

/***/
function deleteEvent() {
  const eventDelete =[].slice.call(document.querySelectorAll('.vtest-event-delete'));

  for (let i = 0; i < eventDelete.length; i++) {
    try {
      eventDelete[i].removeEventListener('click', (e) => {}, true);
    } catch(error){
    }

    eventDelete[i].addEventListener('click', function({target}) {
       target.closest('fieldset').remove();
    });
  }
}

/***/
function workflowAddBtn() {
  const workflowAdd =[].slice.call(document.querySelectorAll('.vtest-workflow-add'));

  for (let i = 0; i < workflowAdd.length; i++) {
    try {
      workflowAdd[i].removeEventListener('click', (e) => {}, true);
    } catch(error){
    }
    workflowAdd[i].addEventListener('click', function({target}) {
      const field = target.closest('fieldset');
      const workflowContainer = field.querySelector('.vtest-workflow');
      workflowContainer.insertAdjacentHTML('beforeend',workflowHTML([_workflow]));
      deleteEvent();
    });
  }
}


/**
 * 
 */
async function init() {
  const wraper = document.getElementById('vtest-extension-wrapper');
  const container = wraper.querySelector('.vtest-variations');
  const addBtn = document.getElementById('add-variation');
  const saveBtn = wraper.querySelector('.vtest-save');
  let conf = JSON.parse(await getDirectoryEntry('conf/demo.json'));
  const storage = localStorage.getItem(STORAGE_KEY);
  
  if(typeof storage == 'string') {
    conf = JSON.parse(storage);
  }

  for (let index = 0; index < Object.keys(conf).length; index++) {
    const vConf = conf[ Object.keys(conf)[index]];
    container.insertAdjacentHTML('beforeend', fieldsetHTML(Object.keys(conf)[index], vConf));
  }

  setTimeout(() => {
    runBtn();
    deleteBtn();
    workflowAddBtn();
    deleteEvent();
  }, 200);

  addBtn.addEventListener('click', function({target}) {
    container.insertAdjacentHTML('beforeend', fieldsetHTML('v', _vConf['v']));
    runBtn();
    deleteBtn();
    workflowAddBtn();
    deleteEvent();
  })

  saveBtn.addEventListener('click', function() {
    savejson();
  })
}

init();
