he easiest is to install http-server globally using node's package manager:

npm install -g http-server

Then simply run http-server in any of your project directories:

C:\Users\d.murillo.porras\OneDrive - Accenture\clearHead>http-server
Starting up http-server, serving ./
Available on:
  http://10.5.170.233:8081
  http://127.0.0.1:8081
Hit CTRL-C to stop the server