function throttle(func, wait) {
  let context, args, timeout, throttling, more, result;
  const whenDone = debounce(function () {
    more = throttling = false;
  }, wait);
  return function () {
    context = this;
    args = arguments;
    const later = function () {
      timeout = null;
      if (more) func.apply(context, args);
      whenDone();
    };
    if (!timeout) timeout = setTimeout(later, wait);
    if (throttling) {
      more = true;
    } else {
      result = func.apply(context, args);
    }
    whenDone();
    throttling = true;
    return result;
  };
};