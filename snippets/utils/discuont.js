/* apply a discuont the fist digits fount for a given element innerHTML
  @param {DOM element} element
  @param {number} discount The desired rate 
  @return element new innerHTML
*/
export function getDiscount(element, discount) {
  const price = element.innerHTML;
  const sale = price.match(/[\d|,|.|\+]+/g)[0];
  let offPrice = parseInt(sale.replace(',','')) - ((parseInt(sale.replace(',',''))*discount)/100);
  if(offPrice % 1 === 0) {
    offPrice = Math.round(offPrice);
  } else {
    offPrice = parseFloat(offPrice).toFixed(2);
  }
  return price.replace(sale, offPrice).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}