/*
  transfor collection of elements into Unencoded text
  support :
  - string
  - html ul>li>span
  @param {array} collection
  @return {string} Unencoded text
*/
export function parseInnnerText(collection = []) {
  let paragraph = "";
  collection.forEach((element) => {
    //string
    if(typeof element === "string") {
      paragraph += `${element}&#10;&#10;`;
    }
    //html: ul > li > span
    if(typeof element === "object" && element.nodeName === "UL") {
      let elementArray = [].slice.call(element.querySelectorAll('li span'));
      let paragraphPiece = "";
      elementArray.reduce((result, item) => {
        paragraphPiece += `&mdash; ${item.innerText}&#10;`; 
        return result;
      }, 0);
      paragraph += `${paragraphPiece}&#10;&#10;`;
    }
  });
  return paragraph.replace('&#10;&#10;&#10;','&#10;&#10;');
}