// Notify me of style changes
const observerConfig = {
  attributes: true, 
  attributeFilter: ["style"]
};
const observer = new MutationObserver(function(mutations) {
  mutations.forEach(function(mutation) {
    if(mutation.attributeName === 'style'){
      waitForElement(`.${PREFIX}-promo-legal-copy`).then((legalCopy) => {
        const cssCheck = document.createElement("DIV");
        cssCheck.style.top = `calc(${pkgSummary.style.top} + 450px)`;
        legalCopy.style.top = cssCheck.style.top;
      });
    }
  });
});

observer.observe(pkgSummary, observerConfig);