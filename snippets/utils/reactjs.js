/**
 * 
*/
export function getReactElement(dom) {

  for (var key in dom) {
    if (key.startsWith('__reactInternalInstance$')) {
     return dom[key];
    }
  }
  return null;
};