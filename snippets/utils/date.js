/**
* 
*/
const stnDate = new Date(bulletOne.innerText.match(/(\d{1,2})\/(\d{1,2})\/(\d{4})+/g)[0]);
const stnMonth = stnDate.toLocaleString('en-US', { month: 'short'});
const stnDay = stnDate.toLocaleString('en-US', { day: 'numeric'});
const suffix = (stnDay % 10 == 1 && stnDay != 11) ? "st"
: (stnDay % 10 == 2 && stnDay != 12) ? "nd"
: (stnDay % 10 == 3 && stnDay != 13) ? "rd"
: "th";
