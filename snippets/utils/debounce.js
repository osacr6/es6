function debounce(func, wait, immediate) {
  let timeout;
  return function () {
    const context = this,
      args = arguments;
    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    if (immediate && !timeout) func.apply(context, args);
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
};



/**
*
*/

function debounce(func){
  var timer;
  return function(event){
    if(timer) clearTimeout(timer);
    timer = setTimeout(func,200,event);
  };
}


window.addEventListener('resize', debounce(function(e){

  [].slice.call(document.querySelectorAll(`.${PREFIX}-recipes`)).forEach( (t) =>  {
    t.remove();
  });
}));