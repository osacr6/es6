/**
waitForAll("#test").then(function(element) {
});
*/
export function waitForAll( selector ) {
  return new Promise(function(resolve, reject) {
    var elements = document.querySelectorAll(selector);

    if(elements.length > 0) {
      resolve(elements);
      return;
    }

    var observer = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        var nodes = Array.from(mutation.addedNodes);
        for(var node of nodes) {
          if(node.matches && node.matches(selector)) {
            observer.disconnect();
            resolve(node);
            return;
          }
        };
      });
    });

    observer.observe(document.documentElement, { childList: true, subtree: true });
  });
}
