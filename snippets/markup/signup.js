
export default function signupFlow(PREFIX) {
  return `
  <section class="${PREFIX}-homepage-plans-pricing">
    <div class="container">
      <div class="${PREFIX}-plan-selector">

        <h1>1. Choose your menu</h1>

        <ul class="${PREFIX}-serving-ctrl" data-selected="0">
          <li class="two-person selected">2-SERVING</li>
          <li class="four-person">4-SERVING</li>
        </ul>
  
        <div class="${PREFIX}-plan-info-wrap">
  
          <div class="${PREFIX}-plan-info ${PREFIX}-plan-2-person selected" data-serving="2" data-id="1" data-index="0" style="display: block;">
            <div class="${PREFIX}-plan-image"></div>
            <div class="${PREFIX}-plan-contents">
              <div class="${PREFIX}-plan-contents--container">
                <h1 class="${PREFIX}-plan-name">Signature</h1>
                <p class="${PREFIX}-plan-desc">Choose from an ever-changing mix of meat, fish, Beyond Meat®, and vegetarian
                  recipes along with repeating customer favorites.</p>
              </div>
            </div>
          </div>

          <div class="${PREFIX}-plan-info ${PREFIX}-plan-vege" data-serving="2" data-id="1" data-index="0" style="display: block;">
            <div class="${PREFIX}-plan-image"></div>
            <div class="${PREFIX}-plan-contents">
              <div class="${PREFIX}-plan-contents--container">
                <h1 class="${PREFIX}-plan-name">Vegetarian</h1>
                <p class="${PREFIX}-plan-desc">Meat-free dishes that celebrate the best of seasonal produce.</p>
              </div>
            </div>
          </div>
  
          <div class="${PREFIX}-plan-info ${PREFIX}-plan-family" data-serving="4" data-id="3" data-index="1" style="display: none;">
            <div class="${PREFIX}-plan-image"></div>
            <div class="${PREFIX}-plan-contents">
              <div class="${PREFIX}-plan-contents--container">
                <h1 class="${PREFIX}-plan-name">Signature for 4</h1>
                <p class="${PREFIX}-plan-desc">Recipes for families, get-togethers, or those who prefer to knock out multiple
                  meals in a single cooking session.</p>
              </div>
            </div>
          </div>
  
        </div>
  
        <h1>2. Recipes per week</h1>
  
        <div class="${PREFIX}-plan-price-wrap">

          <div class="${PREFIX}-plan-pricing-wrap selected" data-serving="2" data-id="1" data-index="0" data-quantity="2" style="display: block;">
            <div class="${PREFIX}-plan-pricing">
              <div>
                <div class="${PREFIX}-recipes-no">2 Recipes</div>
                <div class="${PREFIX}-plan-price">
                  <strong class="${PREFIX}-plan-price-amount">$9.99</strong> per serving
                </div>
                <div class="${PREFIX}-plan-price">
                  <strong class="${PREFIX}-plan-price-amount">$7.99</strong> shipping
                </div>
              </div>
              <div class="${PREFIX}-plan-price ${PREFIX}-plan-price--total">
                <h2 class="${PREFIX}-plan-price-amount">$47.95</h2>
                <p class="${PREFIX}-plan-text">weekly total</p>
              </div>
            </div>
          </div>

          <div class="${PREFIX}-plan-pricing-wrap" data-serving="2" data-id="1" data-index="0" data-quantity="3" style="display: block;">
            <div class="${PREFIX}-plan-pricing">
              <div>
                <div class="${PREFIX}-recipes-no">3 Recipes</div>
                <div class="${PREFIX}-plan-price">
                  <strong class="${PREFIX}-plan-price-amount">$9.99</strong> per serving
                </div>
                <div class="${PREFIX}-plan-price">
                  <strong class="${PREFIX}-plan-price-amount">FREE</strong> shipping
                </div>
              </div>
              <div class="${PREFIX}-plan-price ${PREFIX}-plan-price--total">
                <h2 class="${PREFIX}-plan-price-amount">$59.94</h2>
                <p class="${PREFIX}-plan-text">weekly total</p>
              </div>
            </div>
          </div>
  
          <div class="${PREFIX}-plan-pricing-wrap" data-serving="4" data-id="3" data-index="1" data-quantity="2" style="display: none;">
            <div class="${PREFIX}-plan-pricing">
              <div>
                <div class="${PREFIX}-recipes-no">2 Recipes</div>
                <div class="${PREFIX}-plan-price">
                  <strong class="${PREFIX}-plan-price-amount">$8.99</strong> per serving
                </div>
                <div class="${PREFIX}-plan-price">
                  <strong class="${PREFIX}-plan-price-amount">FREE</strong> shipping
                </div>
              </div>
              <div class="${PREFIX}-plan-price ${PREFIX}-plan-price--total">
                <h2 class="${PREFIX}-plan-price-amount">$71.92</h2>
                <p class="${PREFIX}-plan-text">weekly total</p>
              </div>
            </div>
          </div>
  
          <div class="${PREFIX}-plan-pricing-wrap" data-serving="4" data-id="3" data-index="1" data-quantity="3" style="display: none;">
            <div class="${PREFIX}-plan-pricing">
              <div>
                <div class="${PREFIX}-recipes-no">3 Recipes</div>
                <div class="${PREFIX}-plan-price">
                  <strong class="${PREFIX}-plan-price-amount">$7.99</strong> per serving
                </div>
                <div class="${PREFIX}-plan-price">
                  <strong class="${PREFIX}-plan-price-amount">FREE</strong> shipping
                </div>
              </div>
              <div class="${PREFIX}-plan-price ${PREFIX}-plan-price--total">
                <h2 class="${PREFIX}-plan-price-amount">$95.88</h2>
                <p class="${PREFIX}-plan-text">weekly total</p>
              </div>
            </div>
          </div>
  
          <div class="${PREFIX}-plan-pricing-wrap" data-serving="4" data-id="3" data-index="1" data-quantity="4" style="display: none;">
            <div class="${PREFIX}-plan-pricing">
              <div>
                <div class="${PREFIX}-recipes-no">4 Recipes</div>
                <div class="${PREFIX}-plan-price">
                  <strong class="${PREFIX}-plan-price-amount">$7.49</strong> per serving
                </div>
                <div class="${PREFIX}-plan-price">
                  <strong class="${PREFIX}-plan-price-amount">FREE</strong> shipping
                </div>
              </div>
              <div class="${PREFIX}-plan-price ${PREFIX}-plan-price--total">
                <h2 class="${PREFIX}-plan-price-amount">$119.84</h2>
                <p class="${PREFIX}-plan-text">weekly total</p>
              </div>
            </div>
          </div>
  
          <div class="${PREFIX}-plan-pricing-wrap" data-serving="2" data-id="5" data-index="3" data-quantity="2" style="display: none;">
            <div class="${PREFIX}-plan-pricing">
              <div>
                <div class="${PREFIX}-recipes-no">2 Recipes</div>
                <div class="${PREFIX}-plan-price">
                  <strong class="${PREFIX}-plan-price-amount">$9.99</strong> per serving
                </div>
                <div class="${PREFIX}-plan-price">
                  <strong class="${PREFIX}-plan-price-amount">$7.99</strong> shipping
                </div>
              </div>
              <div class="${PREFIX}-plan-price ${PREFIX}-plan-price--total">
                <h2 class="${PREFIX}-plan-price-amount">$47.95</h2>
                <p class="${PREFIX}-plan-text">weekly total</p>
              </div>
            </div>
          </div>
  
          <div class="${PREFIX}-plan-pricing-wrap" data-serving="2" data-id="5" data-index="3" data-quantity="3" style="display: none;">
            <div class="${PREFIX}-plan-pricing">
              <div>
                <div class="${PREFIX}-recipes-no">3 Recipes</div>
                <div class="${PREFIX}-plan-price">
                  <strong class="${PREFIX}-plan-price-amount">$9.99</strong> per serving
                </div>
                <div class="${PREFIX}-plan-price">
                  <strong class="${PREFIX}-plan-price-amount">FREE</strong> shipping
                </div>
              </div>
              <div class="${PREFIX}-plan-price ${PREFIX}-plan-price--total">
                <h2 class="${PREFIX}-plan-price-amount">$59.94</h2>
                <p class="${PREFIX}-plan-text">weekly total</p>
              </div>
            </div>
          </div>
  
        </div>
  
        <button class="btn btn--full ${PREFIX}-plan-select-btn">CONTINUE</button>
      </div>
    </div>
  </section>`;
}