import {
  setCookie
} from './utils/cookie';

const hasMobileVariants = (/16355890497|16375090390/).test(window.location.search);
const hasDesktopVariants = (/16355990507|16381080447/).test(window.location.search);

if ((hasMobileVariants || hasDesktopVariants) && (/optimizely_x\=/).test(window.location.search)) {
  setCookie('ods-19.6.4-qa', true, 240);
}

const utils = window.optimizely.get('utils');

utils.waitUntil(() => {
  return window.$ && typeof window.$ === 'function';
}).then(() => {
  const $ = window.$;


  utils.waitForElement('.ods-1906-4_chart-updates--email').then(email => {
    $(email).on('click', e => {
      e.preventDefault();
      window['optimizely'] = window['optimizely'] || [];
      window['optimizely'].push({
        type: "event",
        eventName: "ODS_EMAIL_CTA"
      });

      setTimeout(() => {
        window.location = e.target.getAttribute('href');
      }, 250);

    });
  });

  utils.waitForElement('.bankRates').then(bankRates => {

    $(bankRates).on('click', 'a', e => {
      e.preventDefault();
      window['optimizely'] = window['optimizely'] || [];
      window['optimizely'].push({
        type: "event",
        eventName: "ODS_RATES_SECTION_LINKS_CLICK"
      });

      setTimeout(() => {
        window.location = e.currentTarget.getAttribute('href');
      }, 250);
    });
  });

  utils.waitForElement('.ods-1906-4_currencies').then(currency => {
    document.querySelector('.ods-1906-4_currencies .ods-1906-4_currencies-currency-1 a').addEventListener('click',

      function (e) {
        e.preventDefault();
        e.stopPropagation();
        window['optimizely'] = window['optimizely'] || [];
        window['optimizely'].push({
          type: "event",
          eventName: "ODS_CURRENCY_SECTION_LINKS_CLICK"
        });

        setTimeout(() => { 
          window.location = e.target.getAttribute('href');
        }, 300);
      }

    );


    document.querySelector('.ods-1906-4_currencies .ods-1906-4_currencies-currency-2 a').addEventListener('click',

      function (e) {
        e.preventDefault();
        e.stopPropagation();
        window['optimizely'] = window['optimizely'] || [];
        window['optimizely'].push({
          type: "event",
          eventName: "ODS_CURRENCY_SECTION_LINKS_CLICK"
        });

        setTimeout(() => { 
          window.location = e.target.getAttribute('href');
        }, 300);
      } 
    ); 
  });

  utils.waitForElement('.currencySnippet-wrapper').then(currency => {
    $('.currencySnippet-wrapper').on('click', 'a', function (e) {
      e.preventDefault();
      window['optimizely'] = window['optimizely'] || [];
      window['optimizely'].push({
        type: "event",
        eventName: "ODS_CURRENCY_SECTION_LINKS_CLICK"
      });

      setTimeout(() => {
        window.location = e.currentTarget.getAttribute('href');
      }, 300);
    });
  });
});
  
utils.waitForElement('#internalAdSlot1').then(function (element) { 
  utils.waitUntil(function () {
    return element.contentDocument.querySelector('a[href*="/xemoneytransfer/?"]');
  }).then(() => {
    element.contentDocument.querySelector('a[href*="/xemoneytransfer/?"]').addEventListener('click', function () {
      window['optimizely'] = window['optimizely'] || [];
      window['optimizely'].push({
        type: "event",
        eventName: "ods_transfer_from_cta"
      });
    }); 
  });
}); 

utils.waitForElement('.ods-1906-4_chart-updates--transfer').then(function(element) { 

  element.addEventListener('click', function(){
    window['optimizely'] = window['optimizely'] || [];
    window['optimizely'].push({
      type: "event",
      eventName: "ods_transfer_from_cta"
    });
  });
}); 