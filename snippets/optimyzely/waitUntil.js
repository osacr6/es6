const UTILS = window.optimizely.get('utils');

//  Use this when you need to wait for multiple things on the page
UTILS.waitUntil(function() {
  return window.example && document.querySelector('.example') && void 0 !== window.$;
}).then(() => {
  // Do something

});

//  Use this when you need to wait for an element
UTILS.waitForElement('main.example').then(function(element) {
  const example = element.querySelector('.children');
}); 

