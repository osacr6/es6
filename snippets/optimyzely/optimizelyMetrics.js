window['optimizely'] = window['optimizely'] || [];
const UTILS = window["optimizely"].get('utils');
const STATE = window["optimizely"].get("state");
const PREFIX = 'ods-1909-1';
const experimentIdMobile = '16835070408';
const experimentIdDesktop = '16850940385';

// wait for pageWrap load
UTILS.waitForElement('#pageWrap').then(function (pageWrap) {
  const jobPostBtn = pageWrap.querySelector('#jobPostBtn');
  const variationMap = STATE.getVariationMap();
  let mobileVariationState = variationMap[experimentIdMobile] ? variationMap[experimentIdMobile].name : "";
  let desktopVariationState = variationMap[experimentIdDesktop] ? variationMap[experimentIdDesktop].name : "";

  if(mobileVariationState.includes("v0") || desktopVariationState.includes("v0") ) {
    jobPostBtn.addEventListener('click', e => {
      window['optimizely'].push({
        type: "event",
        eventName: "ods_click_on_main_cta"
      })
    });
  } else if(mobileVariationState.includes("v1") || desktopVariationState.includes("v1")) {
    // waitFor for modal
    UTILS.waitForElement(`.${PREFIX}-continue`).then(function (modalCTA) {
      console.log(modalCTA);
      modalCTA.addEventListener('click', e => {
        window['optimizely'].push({
          type: "event",
          eventName: "ods_click_on_main_cta"
        })
      });
    });
  }

});